
#include <fstream>
#include <regex>
#include <thread>
#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>
#include <rcd/settings.hpp>


using namespace std;
using namespace std::chrono;
using namespace boost::filesystem;
using namespace boost::program_options;


namespace rcd
{

//
// Load command line options.
settings::settings(int argc, char *argv[], string &help_message) :
	shutdown_timeout(10),
	thread_pool_size(max<size_t>(2, thread::hardware_concurrency()))
{
	uint32_t timeout = 0;
	string config_path;

	help_message.clear();

	options_description generic("Allowed options");
	generic.add_options()
		("help,h", "Display help message")
		("timeout,t", value<uint32_t>(&timeout), "Set process timeout (in milliseconds)")
		("log,l", "Enable logging for each session")
		("config", value<string>(&config_path)->required(), "Specify file to load list of allowed executables");

	positional_options_description config_option;
	config_option.add("config", 1);

	variables_map vm;
	store(command_line_parser(argc, argv).options(generic).positional(config_option).run(), vm);

	if(vm.count("help"))
	{
		ostringstream buffer;
		generic.print(buffer);
		help_message  = buffer.str();
		return;
	}

	vm.notify();

	if(vm.count("log")) create_log_path();
	process_timeout = milliseconds(timeout);
	load_allowed_executables(config_path);
}


//
// Calculate log path and create corresponding directory.
void settings::create_log_path()
{
	auto logs = current_path() / "logs";
	bool valid = false;

	if(exists(logs))
	{
		if(is_directory(logs)) valid = true;
	}
	else
	{
		if(create_directory(logs)) valid = true;
	}

	if(valid) log_path = logs;
}


//
// Loads list of commands and corresponding execuables.
void settings::load_allowed_executables(const string &file_path)
{
	ifstream file(file_path);
	regex rx("(\\w+)[ \t]+(.+)");
	smatch match;

	string line;
	while(getline(file, line))
	{
		if(regex_search(line, match, rx) && match.size() == 3)
		{

			string command = boost::trim_copy(string(match[1]));
			string executable = boost::trim_copy(string(match[2]));

			if (exists(executable) && !is_directory(executable))
				allowed_executables[command] = executable;
		}
	}
}


} // rcd
