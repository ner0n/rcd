
#ifndef RCD_SESSION_HPP
#define RCD_SESSION_HPP

#include <array>
#include <memory>
#include <iosfwd>
#include <boost/asio.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/filesystem.hpp>
#include <rcd/process.hpp>

namespace rcd
{

//
// Forward declaration.
class server;

//
// Manages client connection.
class session :
	public std::enable_shared_from_this<session>
{
private:

	// Server.
	server & server_;

	// Connected socket.
	boost::asio::ip::tcp::socket socket_;
	// We'll wrap every call into strand to avoid synchronization issues.
	boost::asio::io_service::strand	strand_;
	// Read buffer, used for reading from client only.
	boost::asio::streambuf read_buffer_;
	// Process handle.
	std::unique_ptr<process> process_;
	// Process timer.
	boost::asio::steady_timer timer_;
	// Process buffer, used for reading from process and sending to client.
	std::array<char, 1024> process_buffer_;
	// Logger.
	std::unique_ptr<std::ostream> logger_;

	// Statistics.
	size_t bytes_received_;
	size_t bytes_sent_;

	// Scheduling operations.
	void schedule_socket_receive();
	void schedule_socket_send(size_t size);
	void schedule_process_receive();
	void schedule_process_terminate(std::chrono::milliseconds timeout);

	// Process management.
	void start_process(const std::string &exec, const std::string &args);
	void terminate_process(process::state reason);
	void clean_process();

	// Misc.
	std::ostream* create_logger(const boost::filesystem::path &log_path);
	void parse_user_request(const std::string &request);


	// Forbid copy and assign.
	session(const session &) = delete;
	session& operator=(const session &) = delete;

public:

	using pointer = std::shared_ptr<session>;

	// Create session and start i/o.
	static pointer create(server &srv, boost::asio::ip::tcp::socket socket);

	// Create/destroy.
	session(server &srv, boost::asio::ip::tcp::socket socket);
	~session();

	// Server notifies session about shutdown.
	void notify_server_shutdown();

}; // session

} // rcd

#endif // RCD_SESSION_HPP
