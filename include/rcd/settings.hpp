#ifndef RCD_SETTINGS_HPP
#define RCD_SETTINGS_HPP

#include <map>
#include <string>
#include <chrono>
#include <boost/filesystem.hpp>

namespace rcd
{

//
// Manages server settings.
class settings
{
public:

    // Timeout for graceful server shutdown.
    // Default: 10 seconds.
    std::chrono::seconds shutdown_timeout;

    // Number of threads for event handling.
    // Default: number of cores but not less than 2.
    std::size_t thread_pool_size;

    // Path to store log files in.
    // Default: empty (no log files).
    boost::filesystem::path log_path;

    // Timeout for user process shutdown (in milliseconds).
    // Default: 0 (no timeout).
    std::chrono::milliseconds process_timeout;

    // List of allowed commands (command -> path to executable).
    // Example: ("ping" -> "/bin/ping") or ("cl" -> "C:\Program Files\Visual Studio\cl.exe").
    std::map<std::string, std::string> allowed_executables;

	// Parse command line.
	// If help_message is not empty after the call, --help option has been found.
	// TODO: make something not so clumsy. :(
	settings(int argc, char *argv[], std::string &help_message);

	// Copy settings.
	settings(const settings &) = default;
	settings& operator=(const settings &) = default;

private:

	void create_log_path();
	void load_allowed_executables(const std::string &);

}; // settings

} // rcd

#endif // RCD_SETTINGS_HPP
