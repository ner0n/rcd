#ifndef RCD_SERVER_HPP
#define RCD_SERVER_HPP

#include <thread>
#include <boost/asio.hpp>
#include <rcd/settings.hpp>
#include <rcd/session_manager.hpp>

namespace rcd
{

//
// Controls daemon.
class server
{
private:

    settings settings_;

    boost::asio::io_service service_;
    boost::asio::signal_set signals_;
    boost::asio::ip::tcp::acceptor acceptor_;
    boost::asio::ip::tcp::socket socket_;
    std::unique_ptr<boost::asio::io_service::work> idle_work_;

	std::set<session::pointer> sessions_;
	std::mutex sessions_lock_;

	boost::asio::steady_timer shutdown_timer_;
	std::atomic<bool> is_shutting_down_;


public:

    enum: uint16_t { default_port = 12345 };

	server(const settings &);
	//~server();

    void run();
    void run(std::size_t threads_count);
	void notify_session_close(session::pointer);

	bool is_shutting_down() const;
	const settings & get_settings() const;

private:

	void schedule_signals();
	void schedule_accept();
	void schedule_shutdown();

}; // server

} // rcd

#endif // RCD_SERVER_HPP
