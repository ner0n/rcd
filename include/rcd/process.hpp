#ifndef RCD_PROCESS_HPP
#define RCD_PROCESS_HPP

#include <boost/asio.hpp>
#include <boost/process.hpp>
#include <boost/process/mitigate.hpp>

namespace rcd
{

class process
{
public:

	enum class state : int
	{
		normal = 0,				// Either running or exited
		terminated_shutdown,	// Terminated when server is shutting down
		terminated_timeout,		// Terminated on timeout
		terminated_disconnect,	// Terminated when user disconnected
	};

private:

	// Handle / PID
	boost::process::child handle_;
	// Pipe.
	boost::process::pipe pipe_;
	// Save state for logging.
	state state_;

public:

	// STDOUT redirected here.
	boost::process::pipe_end output;

	process(const std::string& exec, const std::string &args, boost::asio::io_service &srv); // throws
	~process();

	// Terminate process, specify reason.
	void terminate(state reason);
	state get_state();



private:

	// Create asynchronous pipe.
	boost::process::pipe create_pipe();

}; // process

} // rcd

#endif // RCD_PROCESS_HPP
