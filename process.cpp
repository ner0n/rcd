
#include <sstream>
#include <boost/iostreams/device/file_descriptor.hpp>
#include <rcd/process.hpp>

using namespace std;
using namespace boost::iostreams;
using namespace boost::process::initializers;

namespace rcd
{


//
// Launches process, creates pipe, binds it to io_service.
process::process(const string &exec, const string &args, boost::asio::io_service &srv) :
	handle_(0),
	pipe_(create_pipe()),
	state_(state::normal),
	output(srv, pipe_.source)
{
	// Format command line.
	// TODO: make something not so clumsy.
	std::ostringstream cmd_line;
	cmd_line << '"' << exec << '"' << ' ' << args;

	file_descriptor_sink sink(pipe_.sink, close_handle);
	handle_ = boost::process::execute(
		run_exe(exec),
		set_cmd_line(cmd_line.str()),
		bind_stdout(sink),
		close_stdin(),
		close_stderr(),
		hide_console(),
		throw_on_error());
}


//
// Terminate process.
void process::terminate(state reason)
{
	boost::system::error_code ec;
	boost::process::terminate(handle_, ec);
	state_ = reason;
}

//
// Clean up resources.
process::~process()
{
	boost::system::error_code ec;
	boost::process::wait_for_exit(handle_, ec);
}

//
// Returns state.
process::state process::get_state()
{
	return state_;
}


boost::process::pipe process::create_pipe()
{
	return boost::process::create_pipe();
}

} // rcd
