#include <iostream>
#include <thread>
#include <vector>
#include <set>
#include <array>
#include <mutex>
#include <queue>
#include <string>
#include <functional>
#include <algorithm>
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <boost/process.hpp>
#include <boost/process/mitigate.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/device/file_descriptor.hpp>
#include <boost/format.hpp>


#include <rcd/settings.hpp>
#include <rcd/process.hpp>
#include <rcd/server.hpp>


//#include <rcd/logger.hpp>
//#include <rcd/child_process.hpp>
//#include <rcd/settings.hpp>
//#include <rcd/session.hpp>
//#include <rcd/session_manager.hpp>
//#include <rcd/server.hpp>


int main(int argc, char *argv[])
{
    try
    {
		std::string help;
		rcd::settings settings(argc, argv, help);

		if(!help.empty())
		{
			std::cout << help << std::endl;
			return EXIT_SUCCESS;
		}

		std::cout << "Loaded server settings:" << std::endl;
		std::cout << "  Thread pool size:\t" << settings.thread_pool_size << std::endl;
		std::cout << "  Shutdown timeout:\t" << settings.shutdown_timeout.count() << " sec" << std::endl;
		std::cout << "  Log path:\t\t" << settings.log_path << std::endl;
		std::cout << "  Process timeout:\t" << settings.process_timeout.count() << " ms" << std::endl;
		std::cout << "  Allowed executables:" << std::endl;
		for(auto &p: settings.allowed_executables)
		{
			std::cout << "    " << p.first << " -> " << p.second << std::endl;
		}

		rcd::server srv(settings);
		srv.run();

/*
		boost::asio::io_service srv;

		std::string cmd = "/bin/cat";
		std::string args = "/etc/fstab";

		std::unique_ptr<rcd::process> proc(new rcd::process(cmd, args, srv));

		std::array<char, 1024> buffer;
		std::function<void()> do_read = [&]
		{
			boost::asio::async_read(proc->output, boost::asio::buffer(buffer), boost::asio::transfer_at_least(1),
				[&](boost::system::error_code ec, size_t bytes)
				{
					if(!ec)
					{
						//std::cout << "Received " << bytes << " bytes" << std::endl;
						std::string tmp(buffer.data(), bytes);
						std::cout << tmp;

						do_read();
					}
					else
					{
						std::cout << std::endl << ec.message() << std::endl;
					}
				});

		};

		do_read();
		srv.run();*/

    }

	catch(std::exception &ex)
    {
		std::cerr << "Error: " << ex.what() << std::endl;
		return EXIT_FAILURE;
    }

    return 0;
}

