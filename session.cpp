
#include <ctime>
#include <fstream>
#include <sstream>
#include <string>
#include <regex>
#include <thread>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <rcd/session.hpp>
#include <rcd/server.hpp>

using namespace std;
using namespace std::chrono;

// For logging, instead if std::endl.
const char nl = '\n';

/*enum process_reason {
	terminated_shutdown,
	terminated_timeout,
	terminated_disconnect,
};*/


namespace rcd
{

//
// Schedule reading operation on socket.
void session::schedule_socket_receive()
{
	*logger_ << "Scheduling SOCKET RECEIVE operation" << nl;

	auto self(shared_from_this());
	boost::asio::async_read_until(socket_, read_buffer_, '\n',
		strand_.wrap([this, self](boost::system::error_code ec, size_t transferred)
		{
			*logger_ << "Invoking SOCKET RECEIVE callback on thread # " << this_thread::get_id() << nl;
			*logger_ << "Operation result: " << ec.value() << " (" << ec.message() << ")" << nl;

			if (!ec)
			{
				bytes_received_ += transferred;
				*logger_ << "Received: " << transferred << " bytes" << nl;

				string request;
				getline(istream(&read_buffer_), request);
				parse_user_request(request);

				schedule_socket_receive();
			}
			else
			{
				// We get here if:
				// * Network failed (user disconnects, cable unplugged etc).
				// * Server is shutting down and socket was closed.

				terminate_process((ec == boost::asio::error::eof) ?
									  process::state::terminated_disconnect :
									  process::state::terminated_shutdown);
				server_.notify_session_close(self);
			}

			*logger_ << nl;
		}));
}


//
// Schedule writing operation on socket.
void session::schedule_socket_send(size_t size)
{
	*logger_ << "Scheduling SOCKET SEND operation" << nl;

	auto self(shared_from_this());
	boost::asio::async_write(socket_, boost::asio::buffer(process_buffer_, size), boost::asio::transfer_all(),
		strand_.wrap([this, self](boost::system::error_code ec, size_t transferred)
		{
			*logger_ << "Invoking SOCKET SEND callback on thread # " << this_thread::get_id() << nl;
			*logger_ << "Operation result: " << ec.value() << " (" << ec.message() << ")" << nl;

			if (!ec)
			{
				bytes_sent_ += transferred;
				*logger_ << "Sent: " << transferred << " bytes" << nl;

				schedule_process_receive();
			}
			else
			{
				// We get here if:
				// * Network failed (user disconnects, cable unplugged etc).
				// * Server is shutting down and socket was closed.
				// Do nothing, since these errors handled in SOCKET RECEIVE callback.
			}

			*logger_ << nl;
		}));
}


//
// Schedule reading operation on process.
void session::schedule_process_receive()
{
	*logger_ << "Scheduling PROCESS RECEIVE operation" << nl;

	auto self(shared_from_this());
	boost::asio::async_read(process_->output, boost::asio::buffer(process_buffer_), boost::asio::transfer_at_least(1),
		strand_.wrap([this, self](boost::system::error_code ec, size_t transferred)
		{
			*logger_ << "Invoking PROCESS RECEIVE callback on thread # " << this_thread::get_id() << nl;
			*logger_ << "Operation result: " << ec.value() << " (" << ec.message() << ")" << nl;

			if (!ec)
			{
				*logger_ << "Received: " << transferred << " bytes" << nl;

				schedule_socket_send(transferred);
			}
			else
			{
				// We get here if:
				// * Process has been terminated.
				// * Process exited.
				// * Read failure of some sort.

				clean_process();
			}

			*logger_ << nl;
		}));
}


//
// Schedule process termination after timeout.
void session::schedule_process_terminate(milliseconds timeout)
{
	if (!process_) return;
	if (!timeout.count()) return;

	*logger_ << "Scheduling TIMER operation (timeout: " << timeout.count() << " ms)" << nl;

	auto self(shared_from_this());
	timer_.expires_from_now(timeout);
	timer_.async_wait(
		strand_.wrap([this, self](boost::system::error_code ec)
		{
			*logger_ << "Invoking TIMER callback on thread # " << this_thread::get_id() << nl;
			*logger_ << "Operation result: " << ec.value() << " (" << ec.message() << ")" << nl;

			if (!ec)
			{
				terminate_process(process::state::terminated_timeout);
			}
			else
			{
				// We can get here if:
				// * Server is shutting down.
				// * Process exited while we were waiting.
				// * Process exited and user started another process.
			}

			*logger_ << nl;

		}));
}

//
// Parse user request.
void session::parse_user_request(const string &request)
{
	*logger_ << "Processing user request: ";

	// If we have running process, ignore user request.
	if (process_)
	{
		*logger_ << "IGNORED (user already has process running)" << nl;
		return;
	}

	// If server is shutting down, ignore user request.
	if (server_.is_shutting_down())
	{
		*logger_ << "IGNORED (server is shutting down)" << nl;
		return;
	}

	// TODO: write better check.
	regex rx("(\\w+)[ \t]?(.*)");
	smatch match;

	// If incorrect command, ignore user request.
	if (!regex_search(request, match, rx))
	{
		*logger_ << "IGNORED (request is not a command)" << nl;
		return;
	}

	// Extract values.
	string command = boost::trim_copy(string(match[1]));
	string arguments = boost::trim_copy(string(match[2]));

	*logger_ << "FOUND command '" << command << "' which is ";

	auto &list = server_.get_settings().allowed_executables;
	auto it = list.find(command);
	if(it == list.end())
	{
		*logger_ << "FORBIDDEN" << nl;
		return;
	}

	*logger_ << "ALLOWED" << nl;

	// Try to start process.
	start_process(it->second, arguments);
}


//
// Try to start process.
void session::start_process(const string &exec, const string &args)
{
	*logger_ << "Launching PROCESS '" << exec << "' with arguments '" << args << "'" << nl;

	try
	{
		process_.reset(new process(exec, args, socket_.get_io_service()));
	}
	catch(exception &ex)
	{
		*logger_ << "Failed to launch PROCESS: " << ex.what() << nl;
		return;
	}

	// Schedule operations.
	schedule_process_terminate(server_.get_settings().process_timeout);
	schedule_process_receive();
}


//
// Try to terminate process.
void session::terminate_process(process::state reason)
{
	if(!process_) return;

	try
	{
		process_->terminate(reason);
	}
	catch(exception &ex)
	{
		*logger_ << "Failed to terminate PROCESS: " << ex.what() << nl;
	}
}


//
// Clean up resources and write to log.
void session::clean_process()
{
	*logger_ << "Performing PROCESS cleanup." << nl;

	timer_.cancel();
	process_.reset();
}


//
// Write statistics to log.
session::~session()
{
	*logger_ << "TOTAL received: " << bytes_received_ << " bytes" << nl;
	*logger_ << "TOTAL sent: " << bytes_sent_ << " bytes" << nl;
}


//
// Create logger.
std::ostream* session::create_logger(const boost::filesystem::path &log_path)
{
	if(log_path.empty())
	{
		// return new ofstream("/dev/null");
		// return new ofstream("NUL");
		// return new boost::iostreams::stream<boost::iostreams::null_sink>(boost::iostreams::null_sink());

		return new ostringstream;
	}

	// Create file name:
	// YYYY-MM-DD__HH-MM-SS__IP_ADDRESS_PORT.log
	time_t time_data;
	time(&time_data);

	// Date and time.
	char buffer[64] = "";
	strftime(buffer, sizeof(buffer), "%Y-%m-%d__%H-%M-%S__", localtime(&time_data));
	// Host address.
	string host = boost::lexical_cast<string>(socket_.remote_endpoint());
	replace(host.begin(), host.end(), ':', '_');

	string file_name = string(buffer) + host + ".log";
	return new ofstream((log_path / file_name).native());
}


//
// Server is shutting down. Close socket.
void session::notify_server_shutdown()
{
	// We want to use our strand here.

	auto self(shared_from_this());
	strand_.post(
		[this, self]
		{
			*logger_ << "Received SHUTDOWN notification" << nl;
			//terminate_process(terminated_shutdown);
			socket_.close();
		});
}


//
// Constructor.
session::session(server &srv, boost::asio::ip::tcp::socket socket) :
	server_(srv),
	socket_(move(socket)),
	strand_(socket_.get_io_service()),
	timer_(socket_.get_io_service()),
	logger_(create_logger(server_.get_settings().log_path)),
	bytes_received_(0),
	bytes_sent_(0)
{
}


//
// Create session and start i/o.
session::pointer session::create(server &srv, boost::asio::ip::tcp::socket socket)
{
	auto ptr = make_shared<session>(srv, move(socket));
	ptr->schedule_socket_receive();
	return ptr;
}


} // rcd
