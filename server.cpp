

#include <rcd/server.hpp>

using namespace std;

namespace rcd
{

server::server(const settings &options) :
	settings_(options),
	service_(0),
	signals_(service_, SIGINT, SIGTERM),
	acceptor_(service_, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), default_port)),
	socket_(service_),
	idle_work_(new boost::asio::io_service::work(service_)),
	shutdown_timer_(service_),
	is_shutting_down_(false)
{
	schedule_signals();
	schedule_accept();
}

void server::run()
{
	run(settings_.thread_pool_size);
}

void server::run(size_t threads)
{
	vector<thread> pool;
	for(size_t i = 0; i < threads; ++i)
	{
		pool.emplace_back([this]
		{
			while(true)
			{
				try
				{
					service_.run();
					break;
				}
				catch(std::exception &ex)
				{
					cerr << ex.what() << endl;
				}
			}
		});
	}

	for(auto &t: pool) t.join();

}

void server::notify_session_close(session::pointer ptr)
{
	unique_lock<mutex> ul(sessions_lock_);
	sessions_.erase(ptr);
	if(is_shutting_down_)
	{
		if(sessions_.empty()) shutdown_timer_.cancel();
	}
}


bool server::is_shutting_down() const
{
	return is_shutting_down_;
}

const settings & server::get_settings() const
{
	return settings_;
}


void server::schedule_signals()
{
	signals_.async_wait(
		[this](boost::system::error_code ec, int /* signal */)
		{
			if(!ec)
			{
				// Stop accepting connections.
				acceptor_.close();
				// Cancel idle work.
				idle_work_.reset();
				// Initiate session shutdown.
				schedule_shutdown();
			}
			else
			{
				// TODO: add error handling.
				// cerr << ec.message() << endl;
			}
		});
}

void server::schedule_accept()
{
	acceptor_.async_accept(socket_,
		[this](boost::system::error_code ec)
		{
			if(!ec)
			{
				// Successfully accepted connection.
				//manager_.start_session(move(socket_));

				unique_lock<mutex> ul(sessions_lock_);
				sessions_.insert(session::create(*this, move(socket_)));

			}
			else if(ec == boost::asio::error::operation_aborted)
			{
				// Server is shutting down.
				return;
			}
			else
			{
				// TODO: add error handling.
				// cerr << ec.message() << endl;
			}

			// Try to accept new connections.
			schedule_accept();
		});

}


void server::schedule_shutdown()
{
	is_shutting_down_ = true;

	{
		unique_lock<mutex> ul(sessions_lock_);
		if(sessions_.empty()) return;
	}

	shutdown_timer_.expires_from_now(settings_.shutdown_timeout);
	shutdown_timer_.async_wait([this](boost::system::error_code ec)
	{
		if(!ec)
		{
			unique_lock<mutex> ul(sessions_lock_);
			for(auto &s: sessions_) s->notify_server_shutdown();
		}

	});
}



}

